package com.rohan.simplifiedlinkedin.presentation.viewmodel

import androidx.lifecycle.Observer
import com.rohan.simplifiedlinkedin.data.Resource
import com.rohan.simplifiedlinkedin.data.model.Post
import com.rohan.simplifiedlinkedin.data.model.PostEntity
import com.rohan.simplifiedlinkedin.domain.interfaces.HomeRepository
import com.tatadigital.tcp.testrules.BaseTestRule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class HomeViewModelTest: BaseTestRule() {

    @Mock
    lateinit var repository: HomeRepository

    lateinit var viewModel: HomeViewModel

    @Mock
    lateinit var myFeedObserver: Observer<Resource<List<Post>>>

    @Mock
    lateinit var myPostObserver: Observer<Resource<List<Post>>>

    private val userId = "001"


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel =
            HomeViewModel(
                coroutineTestRule.testDispatcher,
                coroutineTestRule.testDispatcher,
                repository
            )
        viewModel.myPosts.observeForever(myPostObserver)
        viewModel.myFeed.observeForever(myFeedObserver)
        Dispatchers.setMain(coroutineTestRule.testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }


    @Test
    fun testViewModelNotNull() = runBlockingTest {
        assertNotNull(viewModel)
    }

    @Test
    fun testCommentListLiveDataNotNull() = runBlockingTest {
        assertNotNull(viewModel.myFeed)
        assertNotNull(viewModel.myPosts)
    }

    @Test
    fun testObserversAttachedToViewModelNotNull() = runBlockingTest {
        assertTrue(viewModel.myPosts.hasObservers())
        assertTrue(viewModel.myFeed.hasObservers())
    }

    @Test
    fun testNotNull() = runBlockingTest {
        Mockito.`when`(
            repository.getMyFeed("")
        ).thenReturn(null)
        assertNotNull(viewModel.myFeed)
        assertNotNull(repository)
        assertTrue(viewModel.myFeed.hasObservers())
    }

    @Test
    fun testNotNull2() = runBlockingTest {
        Mockito.`when`(
            repository.getMyPosts("")
        ).thenReturn(null)
        assertNotNull(viewModel.myPosts)
        assertNotNull(repository)
        assertTrue(viewModel.myPosts.hasObservers())
    }


    @Test
    fun testMyFeedApiFetchDataSuccess() = runBlockingTest {
        val post = Mockito.mock(PostEntity::class.java)

        val data = Resource.success(post.posts)
        Mockito.`when`(
            repository.getMyFeed(userId)
        ).thenReturn(data)

        viewModel.getMyFeed(userId)
        Mockito.verify(myFeedObserver).onChanged(Resource.loading(null))
        Mockito.verify(myFeedObserver).onChanged(data)
    }

    @Test
    fun testMyPostsApiFetchDataSuccess() = runBlockingTest {
        val post = Mockito.mock(PostEntity::class.java)

        val data = Resource.success(post.posts)
        Mockito.`when`(
            repository.getMyPosts(userId)
        ).thenReturn(data)

        viewModel.getMyPost(userId)
        Mockito.verify(myPostObserver).onChanged(Resource.loading(null))
        Mockito.verify(myPostObserver).onChanged(data)
    }

}