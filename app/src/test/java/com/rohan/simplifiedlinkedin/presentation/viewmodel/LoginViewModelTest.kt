package com.rohan.simplifiedlinkedin.presentation.viewmodel

import androidx.lifecycle.Observer
import com.rohan.simplifiedlinkedin.data.Resource
import com.rohan.simplifiedlinkedin.data.model.User
import com.rohan.simplifiedlinkedin.domain.interfaces.LoginRepository
import com.tatadigital.tcp.testrules.BaseTestRule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class LoginViewModelTest: BaseTestRule() {

    @Mock
    lateinit var repository: LoginRepository

    lateinit var viewModel: LoginViewModel

    @Mock
    lateinit var userObserver: Observer<Resource<User>>

    @Mock
    lateinit var loginObserver: Observer<Resource<Boolean>>

    private val mobileNumber = "1234567890"

    private val pass = "paytm"


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel =
            LoginViewModel(
                coroutineTestRule.testDispatcher,
                coroutineTestRule.testDispatcher,
                repository
            )
        viewModel.userInfo.observeForever(userObserver)
        viewModel.isValidCredentials.observeForever(loginObserver)
        Dispatchers.setMain(coroutineTestRule.testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }


    @Test
    fun testViewModelNotNull() = runBlockingTest {
        assertNotNull(viewModel)
    }

    @Test
    fun testCommentListLiveDataNotNull() = runBlockingTest {
        assertNotNull(viewModel.userInfo)
        assertNotNull(viewModel.isValidCredentials)
    }

    @Test
    fun testObserversAttachedToViewModelNotNull() = runBlockingTest {
        assertTrue(viewModel.userInfo.hasObservers())
        assertTrue(viewModel.isValidCredentials.hasObservers())
    }

    @Test
    fun testNotNull() = runBlockingTest {
        Mockito.`when`(
            repository.getUserInfo("")
        ).thenReturn(null)
        assertNotNull(viewModel.userInfo)
        assertNotNull(repository)
        assertTrue(viewModel.userInfo.hasObservers())
    }

    @Test
    fun testNotNull2() = runBlockingTest {
        Mockito.`when`(
            repository.validateCredentials("", "")
        ).thenReturn(null)
        assertNotNull(viewModel.isValidCredentials)
        assertNotNull(repository)
        assertTrue(viewModel.isValidCredentials.hasObservers())
    }

    @Test
    fun testUserInfoApiFetchDataSuccess() = runBlockingTest {
       val user = Mockito.mock(User::class.java)

        val data = Resource.success(user)
        Mockito.`when`(
            repository.getUserInfo(mobileNumber)
        ).thenReturn(data)

        viewModel.getUserInfo(mobileNumber)
        Mockito.verify(userObserver).onChanged(Resource.loading(null))
        Mockito.verify(userObserver).onChanged(data)
    }

    @Test
    fun testCredentialsApiCheckDataSuccess() = runBlockingTest {
        val data = Resource.success(false)
        Mockito.`when`(
            repository.validateCredentials(mobileNumber, pass)
        ).thenReturn(data)

        viewModel.validateCredentials(mobileNumber, pass)
        Mockito.verify(loginObserver).onChanged(Resource.loading(null))
        Mockito.verify(loginObserver).onChanged(data)
    }
}