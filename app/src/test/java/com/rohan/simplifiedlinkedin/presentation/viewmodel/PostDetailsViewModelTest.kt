package com.rohan.simplifiedlinkedin.presentation.viewmodel

import androidx.lifecycle.Observer
import com.rohan.simplifiedlinkedin.data.Resource
import com.rohan.simplifiedlinkedin.data.model.CommentDetail
import com.rohan.simplifiedlinkedin.domain.interfaces.PostDetailsRepository
import com.tatadigital.tcp.testrules.BaseTestRule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

@ExperimentalCoroutinesApi
@RunWith(JUnit4::class)
class PostDetailsViewModelTest: BaseTestRule() {

    @Mock
    lateinit var repository: PostDetailsRepository

    lateinit var viewModel: PostDetailsViewModel

    @Mock
    lateinit var observer: Observer<Resource<List<CommentDetail>>>

    private val postId = "1"

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        viewModel =
            PostDetailsViewModel(
                coroutineTestRule.testDispatcher,
                coroutineTestRule.testDispatcher,
                repository
            )
        viewModel.commentsList.observeForever(observer)
        Dispatchers.setMain(coroutineTestRule.testDispatcher)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }


    @Test
    fun testViewModelNotNull() = runBlockingTest {
        assertNotNull(viewModel)
    }

    @Test
    fun testCommentListLiveDataNotNull() = runBlockingTest {
        assertNotNull(viewModel.commentsList)
    }

    @Test
    fun testObserversAttachedToViewModelNotNull() = runBlockingTest {
        assertTrue(viewModel.commentsList.hasObservers())
    }

    @Test
    fun testNotNull() = runBlockingTest {
        Mockito.`when`(
            repository.getComments("")
        ).thenReturn(null)
        assertNotNull(viewModel.commentsList)
        assertNotNull(repository)
        assertTrue(viewModel.commentsList.hasObservers())
    }


    @Test
    fun testApiFetchDataSuccess() = runBlockingTest {
        val comment = Mockito.mock(CommentDetail::class.java)
        val data = Resource.success(listOf(comment))
        Mockito.`when`(
            repository.getComments(postId)
        ).thenReturn(data)

        viewModel.getComments(postId)
        Mockito.verify(observer).onChanged(Resource.loading(null))
        Mockito.verify(observer).onChanged(data)
    }
}