package com.rohan.simplifiedlinkedin.di.module

import com.rohan.simplifiedlinkedin.presentation.ui.fragment.LoginFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class LoginFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributesLoginFragment(): LoginFragment

}