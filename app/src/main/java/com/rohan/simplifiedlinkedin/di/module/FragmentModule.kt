package com.rohan.simplifiedlinkedin.di.module

import com.rohan.simplifiedlinkedin.presentation.ui.fragment.MyFeedFragment
import com.rohan.simplifiedlinkedin.presentation.ui.fragment.MyPostFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract fun contributesMyFeedFragment(): MyFeedFragment

    @ContributesAndroidInjector
    abstract fun contributesMyPostFragment(): MyPostFragment

}