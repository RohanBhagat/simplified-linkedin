package com.rohan.simplifiedlinkedin.di.module

import com.rohan.simplifiedlinkedin.presentation.ui.fragment.PostDetailsFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class PostDetailsFragmentModule {

    @ContributesAndroidInjector
    abstract fun contributesPostDetailsFragment(): PostDetailsFragment

}