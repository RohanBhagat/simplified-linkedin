package com.rohan.simplifiedlinkedin.di.module

import com.rohan.simplifiedlinkedin.presentation.ui.activity.PostDetailsActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class PostDetailsActivityModule {

    @ContributesAndroidInjector(
        modules = [PostDetailsFragmentModule::class, RepositoryModule::class]
    )
    abstract fun contributesPostDetailsActivity(): PostDetailsActivity

}