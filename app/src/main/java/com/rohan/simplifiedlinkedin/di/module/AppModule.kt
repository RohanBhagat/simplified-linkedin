package com.rohan.simplifiedlinkedin.di.module

import android.content.Context
import com.google.gson.Gson
import com.rohan.simplifiedlinkedin.SimplifiedLinkedInApp
import com.rohan.simplifiedlinkedin.data.remote.ApiService
import com.rohan.simplifiedlinkedin.data.remote.ApiServiceImpl
import dagger.Module
import dagger.Provides

@Module
class AppModule {

    @Provides
    fun provideGson() = Gson()

    @Provides
    fun provideApiService(apiServiceImpl: ApiServiceImpl): ApiService {
        return apiServiceImpl
    }

    @Provides
    fun provideContext(application: SimplifiedLinkedInApp): Context {
        return application
    }
}