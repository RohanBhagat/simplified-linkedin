package com.rohan.simplifiedlinkedin.di.component

import com.rohan.simplifiedlinkedin.SimplifiedLinkedInApp
import com.rohan.simplifiedlinkedin.di.module.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        LoginActivityModule::class,
        MainActivityModule::class,
        SplashActivityModule::class,
        PostDetailsActivityModule::class]
)
interface AppComponent : AndroidInjector<SimplifiedLinkedInApp> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: SimplifiedLinkedInApp): Builder

        fun build(): AppComponent
    }

}