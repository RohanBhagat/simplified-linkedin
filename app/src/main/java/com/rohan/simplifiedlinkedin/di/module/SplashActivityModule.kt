package com.rohan.simplifiedlinkedin.di.module

import com.rohan.simplifiedlinkedin.presentation.ui.activity.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SplashActivityModule {

    @ContributesAndroidInjector
    abstract fun contributesSplashActivity(): SplashActivity

}