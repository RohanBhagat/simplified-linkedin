package com.rohan.simplifiedlinkedin.di.module

import com.rohan.simplifiedlinkedin.presentation.ui.activity.LoginActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class LoginActivityModule {

    @ContributesAndroidInjector(
        modules = [LoginFragmentModule::class, RepositoryModule::class]
    )
    abstract fun contributesLoginActivity(): LoginActivity

}