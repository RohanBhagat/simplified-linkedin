package com.rohan.simplifiedlinkedin.di.module

import com.rohan.simplifiedlinkedin.data.repository.HomeRepositoryImpl
import com.rohan.simplifiedlinkedin.data.repository.LoginRepositoryImpl
import com.rohan.simplifiedlinkedin.data.repository.PostDetailsRepositoryImpl
import com.rohan.simplifiedlinkedin.domain.interfaces.HomeRepository
import com.rohan.simplifiedlinkedin.domain.interfaces.LoginRepository
import com.rohan.simplifiedlinkedin.domain.interfaces.PostDetailsRepository
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Named

@Module
class RepositoryModule {

    @Provides
    fun provideLoginRepository(loginRepositoryImpl: LoginRepositoryImpl): LoginRepository {
        return loginRepositoryImpl
    }

    @Provides
    fun provideHomeRepository(homeRepositoryImpl: HomeRepositoryImpl): HomeRepository {
        return homeRepositoryImpl
    }

    @Provides
    fun providePostDetailsRepository(postDetailsRepositoryImpl: PostDetailsRepositoryImpl): PostDetailsRepository {
        return postDetailsRepositoryImpl
    }

    @Provides
    @Named("IO")
    fun provideIOCoroutineDispatcher(): CoroutineDispatcher {
        return Dispatchers.IO
    }

    @Provides
    @Named("MAIN")
    fun provideMainCoroutineDispatcher(): CoroutineDispatcher {
        return Dispatchers.Main
    }
}