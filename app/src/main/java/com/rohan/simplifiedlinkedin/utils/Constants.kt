package com.rohan.simplifiedlinkedin.utils

object Constants {

    const val GET_FEED_API_PATH: String = "getFeed.json"

    const val GET_USER_INFO_API_PATH = "getUserInfo.json"

    const val GET_COMMENTS_DETAIL_API_PATH = "getComments.json"

    const val POST_DETAILS = "postDetails"
}