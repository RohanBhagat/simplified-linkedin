package com.rohan.simplifiedlinkedin.utils

import android.content.Context
import android.net.ConnectivityManager
import android.view.View
import com.rohan.simplifiedlinkedin.R
import kotlin.random.Random

class CommonFunctions {

    companion object {
        private val validDigitsList = listOf('0', '1', '2', '3', '4', '5', '6', '7', '8', '9')

        private val maleAvatars =
            listOf(R.drawable.ic_male1, R.drawable.ic_male2, R.drawable.ic_male3)
        private val femaleAvatars = listOf(R.drawable.ic_female1, R.drawable.ic_female2)

        fun isValidIndianMobileNumber(mobileNumber: String): Boolean {
            if (mobileNumber.length != 10) {
                return false
            }

            for (c in mobileNumber.toCharArray()) {
                if (c !in validDigitsList) {
                    return false
                }
            }

            return true
        }

        fun showViews(vararg views: View) {
            views.forEach {
                it.visibility = View.VISIBLE
            }
        }

        fun hideViews(vararg views: View) {
            views.forEach {
                it.visibility = View.GONE
            }
        }

        fun isNetworkConnected(context: Context?): Boolean {
            val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
            return cm?.activeNetworkInfo != null && cm.activeNetworkInfo?.isConnected == true
        }

        fun returnMaleAvatar(): Int {
            val random = Random.nextInt(0, 3)
            return maleAvatars[random]
        }

        fun returnFemaleAvatar(): Int {
            val random = Random.nextInt(0, 2)
            return femaleAvatars[random]
        }

    }
}