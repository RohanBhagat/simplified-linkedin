package com.rohan.simplifiedlinkedin.utils

import android.content.Context
import com.google.gson.Gson
import com.rohan.simplifiedlinkedin.data.model.User
import javax.inject.Inject


const val USER_INFO_PREF = "USER_INFO_PREF"
const val USER_INFO_KEY = "USER_INFO_KEY"
const val USER_IS_LOGGED_IN = "USER_IS_LOGGED_IN"

class UserSession @Inject constructor(
    private val context: Context,
    private val gson: Gson
) {

    fun saveUserInfo(user: User) {
        val prefs = context.getSharedPreferences(USER_INFO_PREF, 0)

        val editor = prefs.edit()
        editor.putString(USER_INFO_KEY, gson.toJson(user))
        editor.putBoolean(USER_IS_LOGGED_IN, true)
        editor.apply()
    }

    fun isUserLoggedIn(): Boolean {
        val prefs = context.getSharedPreferences(USER_INFO_PREF, 0)

        return prefs.getBoolean(USER_IS_LOGGED_IN, false)
    }

    fun clearUserSession() {
        val prefs = context.getSharedPreferences(USER_INFO_PREF, 0)

        val editor = prefs.edit()
        editor.putString(USER_INFO_KEY, "")
        editor.putBoolean(USER_IS_LOGGED_IN, false)
        editor.apply()
    }

    fun getUserData(): User {
        val prefs = context.getSharedPreferences(USER_INFO_PREF, 0)

        val value = prefs.getString(USER_INFO_KEY, "")

        if (!value.isNullOrEmpty()) {
            return gson.fromJson(value, User::class.java)
        }

        return User()
    }

}