package com.rohan.simplifiedlinkedin.utils

import android.content.Context
import java.io.BufferedReader
import java.io.InputStream
import java.io.InputStreamReader

class ReadJsonFileHelper {

    companion object {

        @Throws(Exception::class)
        private fun convertStreamToString(`is`: InputStream): String {
            val reader = BufferedReader(InputStreamReader(`is`))
            val sb = StringBuilder()
            var line: String?
            while (reader.readLine().also { line = it } != null) {
                sb.append(line).append("\n")
            }
            reader.close()
            return sb.toString()
        }

        fun readJsonFile(context: Context, filePath: String): String {
            return try {
                val stream = context.assets.open(filePath)
                val jsonString = convertStreamToString(stream)
                stream.close()
                jsonString
            } catch (e: Exception) {
                e.printStackTrace()
                "Couldn't process your request"
            }
        }
    }
}