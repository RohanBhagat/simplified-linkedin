package com.rohan.simplifiedlinkedin.utils

import javax.inject.Inject

class CheckCredentials @Inject constructor() {

    private val userCredentials: HashMap<String, String> = hashMapOf()

    init {
        userCredentials["1234567890"] = "paytm"
        userCredentials["9876543211"] = "987112"
        userCredentials["9876543212"] = "987212"
        userCredentials["9876543213"] = "987312"
        userCredentials["9876543214"] = "987412"
    }

    fun isCredentialsValid(mobile: String, pass: String): Boolean {
        for ((key, value) in userCredentials) {
            if (key == mobile && value == pass) {
                return true
            }
        }

        return false
    }
}