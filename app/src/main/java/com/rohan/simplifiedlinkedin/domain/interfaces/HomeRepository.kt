package com.rohan.simplifiedlinkedin.domain.interfaces

import com.rohan.simplifiedlinkedin.data.Resource
import com.rohan.simplifiedlinkedin.data.model.Post

interface HomeRepository {

    suspend fun getMyFeed(userId: String): Resource<List<Post>>

    suspend fun getMyPosts(userId: String): Resource<List<Post>>
}