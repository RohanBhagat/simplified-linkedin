package com.rohan.simplifiedlinkedin.domain.interfaces

import com.rohan.simplifiedlinkedin.data.Resource
import com.rohan.simplifiedlinkedin.data.model.CommentDetail

interface PostDetailsRepository {

    suspend fun getComments(postId: String): Resource<List<CommentDetail>>

}