package com.rohan.simplifiedlinkedin.domain.interfaces

import com.rohan.simplifiedlinkedin.data.Resource
import com.rohan.simplifiedlinkedin.data.model.User

interface LoginRepository {

    suspend fun getUserInfo(mobileNumber: String): Resource<User>

    suspend fun validateCredentials(mobileNumber: String, password: String): Resource<Boolean>
}