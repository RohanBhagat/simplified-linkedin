package com.rohan.simplifiedlinkedin

import com.rohan.simplifiedlinkedin.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class SimplifiedLinkedInApp : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerAppComponent.builder().application(this).build()

        appComponent.inject(this)

        return appComponent
    }
}