package com.rohan.simplifiedlinkedin.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rohan.simplifiedlinkedin.data.Resource
import com.rohan.simplifiedlinkedin.data.model.Post
import com.rohan.simplifiedlinkedin.domain.interfaces.HomeRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

class HomeViewModel @Inject constructor(
    @Named("IO") private var io: CoroutineDispatcher,
    @Named("MAIN") private var main: CoroutineDispatcher,
    private val homeRepository: HomeRepository
) : ViewModel() {

    private val _myFeed: MutableLiveData<Resource<List<Post>>> = MutableLiveData()
    val myFeed: LiveData<Resource<List<Post>>> = _myFeed

    private val _myPosts: MutableLiveData<Resource<List<Post>>> = MutableLiveData()
    val myPosts: LiveData<Resource<List<Post>>> = _myPosts

    fun getMyFeed(userId: String) {

        var response: Resource<List<Post>>?

        _myFeed.value = Resource.loading()
        viewModelScope.launch {
            withContext(io) {
                response = homeRepository.getMyFeed(userId)
            }

            withContext(main) {
                _myFeed.value = response
            }

        }
    }

    fun getMyPost(userId: String) {

        var response: Resource<List<Post>>?

        _myPosts.value = Resource.loading()
        viewModelScope.launch {
            withContext(io) {
                response = homeRepository.getMyPosts(userId)
            }

            withContext(main) {
                _myPosts.value = response
            }
        }
    }
}