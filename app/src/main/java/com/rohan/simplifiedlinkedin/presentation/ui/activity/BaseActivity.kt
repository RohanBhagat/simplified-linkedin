package com.rohan.simplifiedlinkedin.presentation.ui.activity

import android.os.Bundle
import androidx.core.content.ContextCompat
import com.rohan.simplifiedlinkedin.R
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryDark)
    }
}