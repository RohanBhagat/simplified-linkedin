package com.rohan.simplifiedlinkedin.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rohan.simplifiedlinkedin.R
import com.rohan.simplifiedlinkedin.data.model.CommentDetail
import com.rohan.simplifiedlinkedin.databinding.ItemCommentLayoutBinding
import com.rohan.simplifiedlinkedin.utils.CommonFunctions

object CommentsDiffCallBack : DiffUtil.ItemCallback<CommentDetail>() {
    override fun areItemsTheSame(oldItem: CommentDetail, newItem: CommentDetail): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: CommentDetail, newItem: CommentDetail): Boolean {
        return oldItem.postId == newItem.postId && oldItem.commentDate == newItem.commentDate && oldItem.commentedBy == newItem.commentedBy &&
                oldItem.comment == newItem.comment && oldItem.userId == newItem.userId && oldItem.gender == newItem.gender
    }
}

class CommentAdapter : ListAdapter<CommentDetail, CommentAdapter.ViewHolder>(CommentsDiffCallBack) {

    inner class ViewHolder(val binding: ItemCommentLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: CommentDetail) {

            with(binding) {

                tvComment.text = item.comment

                tvCommentedBy.text = item.commentedBy

                tvDate.text = item.commentDate

                when (item.gender) {
                    "M" -> ivAvatar.setImageResource(CommonFunctions.returnMaleAvatar())
                    "F" -> ivAvatar.setImageResource(CommonFunctions.returnFemaleAvatar())
                    else -> ivAvatar.setImageResource(R.drawable.ic_female1)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ItemCommentLayoutBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_comment_layout,
            parent,
            false
        )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(getItem(position))

}