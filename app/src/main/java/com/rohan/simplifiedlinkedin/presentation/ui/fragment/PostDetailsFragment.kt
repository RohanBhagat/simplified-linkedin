package com.rohan.simplifiedlinkedin.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.rohan.simplifiedlinkedin.R
import com.rohan.simplifiedlinkedin.data.Resource
import com.rohan.simplifiedlinkedin.data.model.Post
import com.rohan.simplifiedlinkedin.databinding.FragmentPostDetailsBinding
import com.rohan.simplifiedlinkedin.presentation.ui.adapter.CommentAdapter
import com.rohan.simplifiedlinkedin.presentation.viewmodel.PostDetailsViewModel
import com.rohan.simplifiedlinkedin.presentation.viewmodel.PostDetailsViewModelFactory
import com.rohan.simplifiedlinkedin.utils.CommonFunctions
import com.rohan.simplifiedlinkedin.utils.Constants.POST_DETAILS
import javax.inject.Inject

class PostDetailsFragment : BaseFragment() {

    private lateinit var binding: FragmentPostDetailsBinding

    private lateinit var postDetailsViewModel: PostDetailsViewModel

    @Inject
    lateinit var postDetailsViewModelFactory: PostDetailsViewModelFactory

    private var post: Post? = null

    private var commentAdapter: CommentAdapter? = null

    companion object {
        @JvmStatic
        fun newInstance(post: Post) =
            PostDetailsFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(POST_DETAILS, post)
                }
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        postDetailsViewModel = ViewModelProvider(
            this,
            postDetailsViewModelFactory
        ).get(PostDetailsViewModel::class.java)

        post = arguments?.getSerializable(POST_DETAILS) as? Post
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_post_details, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (!post?.postId.isNullOrEmpty())
            postDetailsViewModel.getComments(post?.postId ?: "")

        initViews()

        observeData()
    }

    private fun observeData() {

        postDetailsViewModel.commentsList.observe(viewLifecycleOwner, { response ->

            if (response != null) {
                when (response.status) {
                    Resource.Status.LOADING -> {
                        startShimmer()
                    }

                    Resource.Status.SUCCESS -> {
                        response.data?.let {
                            if (response.data.isNotEmpty()) {

                                stopShimmer()
                                CommonFunctions.showViews(binding.rvComments)

                                if (commentAdapter == null) {
                                    commentAdapter = CommentAdapter()
                                    binding.rvComments.layoutManager = LinearLayoutManager(
                                        this.context,
                                        LinearLayoutManager.VERTICAL,
                                        false
                                    )
                                    binding.rvComments.adapter = commentAdapter
                                    binding.rvComments.setHasFixedSize(true)
                                    commentAdapter?.submitList(response.data)
                                } else {
                                    commentAdapter?.submitList(response.data)
                                }
                            } else {
                                CommonFunctions.hideViews(binding.rvComments)
                                stopShimmer()
                                binding.tvComments.text = getString(R.string.no_comments_available)
                            }
                        }
                    }

                    Resource.Status.ERROR -> {

                    }
                }
            }

        })
    }

    private fun initViews() {

        with(binding) {

            tvCommentsCount.text = "${post?.commentsCount} Comments"

            tvLikesCount.text = "${post?.likesCount} Likes"

            tvDate.text = post?.postDate

            tvOwnerName.text = post?.postedBy

            tvPost.text = post?.message

            tvOrganization.text = post?.postOwnerOrganization

            when (post?.gender) {
                "M" -> ivAvatar.setImageResource(CommonFunctions.returnMaleAvatar())
                "F" -> ivAvatar.setImageResource(CommonFunctions.returnFemaleAvatar())
                else -> ivAvatar.setImageResource(R.drawable.ic_female1)
            }
        }
    }

    private fun startShimmer() {
        CommonFunctions.showViews(binding.layoutShimmerView.shimmerView)
        CommonFunctions.hideViews(binding.rvComments)
        binding.layoutShimmerView.shimmerView.startShimmer()
    }

    private fun stopShimmer() {
        CommonFunctions.hideViews(binding.layoutShimmerView.shimmerView)
        binding.layoutShimmerView.shimmerView.stopShimmer()
    }

}