package com.rohan.simplifiedlinkedin.presentation.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rohan.simplifiedlinkedin.R
import com.rohan.simplifiedlinkedin.data.model.Post
import com.rohan.simplifiedlinkedin.databinding.ItemPostLayoutBinding
import com.rohan.simplifiedlinkedin.utils.CommonFunctions

object PostsDiffCallBack : DiffUtil.ItemCallback<Post>() {
    override fun areItemsTheSame(oldItem: Post, newItem: Post): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Post, newItem: Post): Boolean {
        return oldItem.postId == newItem.postId && oldItem.postDate == newItem.postDate && oldItem.postedBy == newItem.postedBy && oldItem.message == newItem.message
                && oldItem.likesCount == newItem.likesCount && oldItem.commentsCount == newItem.commentsCount && oldItem.userId == newItem.userId && oldItem.gender == newItem.gender
    }
}

class PostAdapter(val openPostDetailsActivity: (post: Post) -> Unit) :
    ListAdapter<Post, PostAdapter.ViewHolder>(PostsDiffCallBack) {

    inner class ViewHolder(val binding: ItemPostLayoutBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Post) {

            with(binding) {

                tvCommentsCount.text = "${item.commentsCount} Comments"

                tvLikesCount.text = "${item.likesCount} Likes"

                tvDate.text = item.postDate

                tvOwnerName.text = item.postedBy

                tvPost.text = item.message

                tvOrganization.text = item.postOwnerOrganization

                cvMain.setOnClickListener {
                    openPostDetailsActivity(item)
                }

                when (item.gender) {
                    "M" -> ivAvatar.setImageResource(CommonFunctions.returnMaleAvatar())
                    "F" -> ivAvatar.setImageResource(CommonFunctions.returnFemaleAvatar())
                    else -> ivAvatar.setImageResource(R.drawable.ic_female1)
                }

            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = DataBindingUtil.inflate<ItemPostLayoutBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_post_layout,
            parent,
            false
        )

        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(getItem(position))

}