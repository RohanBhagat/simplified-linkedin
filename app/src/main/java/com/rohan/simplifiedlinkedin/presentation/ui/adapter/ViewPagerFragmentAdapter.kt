package com.rohan.simplifiedlinkedin.presentation.ui.adapter

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.rohan.simplifiedlinkedin.presentation.ui.fragment.MyFeedFragment
import com.rohan.simplifiedlinkedin.presentation.ui.fragment.MyPostFragment

class ViewPagerFragmentAdapter(fragment: Fragment, private val titles: Array<String>) :
    FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = titles.size

    override fun createFragment(position: Int): Fragment {
        when (position) {
            0 -> {
                return MyFeedFragment.newInstance()
            }

            1 -> {
                return MyPostFragment.newInstance()
            }
        }

        return MyFeedFragment()
    }

}