package com.rohan.simplifiedlinkedin.presentation.ui.fragment

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.google.android.material.tabs.TabLayoutMediator
import com.rohan.simplifiedlinkedin.R
import com.rohan.simplifiedlinkedin.databinding.FragmentHomeBinding
import com.rohan.simplifiedlinkedin.presentation.ui.adapter.ViewPagerFragmentAdapter

class HomeFragment : BaseFragment() {

    private lateinit var binding: FragmentHomeBinding

    private val titles = arrayOf("My Feed", "My Posts")

    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {

        binding.tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#DA1C5C"))

        binding.viewPager.adapter = ViewPagerFragmentAdapter(this, titles)

        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, position ->
            tab.text = titles[position]
        }.attach()

    }

}