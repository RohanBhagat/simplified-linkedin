package com.rohan.simplifiedlinkedin.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rohan.simplifiedlinkedin.data.Resource
import com.rohan.simplifiedlinkedin.data.model.User
import com.rohan.simplifiedlinkedin.domain.interfaces.LoginRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

class LoginViewModel @Inject constructor(
    @Named("IO") private var io: CoroutineDispatcher,
    @Named("MAIN") private var main: CoroutineDispatcher,
    private val loginRepository: LoginRepository
) : ViewModel() {

    private val _isValidCredentials: MutableLiveData<Resource<Boolean>> = MutableLiveData()
    val isValidCredentials: LiveData<Resource<Boolean>> = _isValidCredentials

    private val _userInfo: MutableLiveData<Resource<User>> = MutableLiveData()
    val userInfo: LiveData<Resource<User>> = _userInfo

    fun getUserInfo(mobileNumber: String) {

        var response: Resource<User>?

        _userInfo.value = Resource.loading()
        viewModelScope.launch {
            withContext(io) {
                response = loginRepository.getUserInfo(mobileNumber)
            }

            withContext(main) {
                _userInfo.value = response
            }

        }
    }

    fun validateCredentials(mobileNumber: String, password: String) {

        var response: Resource<Boolean>?

        _isValidCredentials.value = Resource.loading()
        viewModelScope.launch {
            withContext(io) {
                response = loginRepository.validateCredentials(mobileNumber, password)
            }

            withContext(main) {
                _isValidCredentials.value = response
            }
        }
    }
}