package com.rohan.simplifiedlinkedin.presentation.ui.activity

import android.os.Bundle
import com.rohan.simplifiedlinkedin.R
import com.rohan.simplifiedlinkedin.presentation.ui.fragment.LoginFragment

class LoginActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_login)

        val loginFragment = LoginFragment.newInstance()
        supportFragmentManager.beginTransaction()
            .add(R.id.containerLayout, loginFragment).commit()
    }
}