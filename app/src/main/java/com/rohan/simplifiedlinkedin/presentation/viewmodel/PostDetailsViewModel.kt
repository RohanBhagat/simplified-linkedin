package com.rohan.simplifiedlinkedin.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rohan.simplifiedlinkedin.data.Resource
import com.rohan.simplifiedlinkedin.data.model.CommentDetail
import com.rohan.simplifiedlinkedin.domain.interfaces.PostDetailsRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

class PostDetailsViewModel @Inject constructor(
    @Named("IO") private var io: CoroutineDispatcher,
    @Named("MAIN") private var main: CoroutineDispatcher,
    private val postDetailsRepository: PostDetailsRepository
) : ViewModel() {

    private val _commentsList: MutableLiveData<Resource<List<CommentDetail>>> = MutableLiveData()
    val commentsList: LiveData<Resource<List<CommentDetail>>> = _commentsList

    fun getComments(postId: String) {

        var response: Resource<List<CommentDetail>>?

        _commentsList.value = Resource.loading()
        viewModelScope.launch {
            withContext(io) {
                response = postDetailsRepository.getComments(postId)
            }

            withContext(main) {
                _commentsList.value = response
            }

        }
    }
}