package com.rohan.simplifiedlinkedin.presentation.ui.activity

import android.os.Bundle
import android.view.MenuItem
import com.rohan.simplifiedlinkedin.R
import com.rohan.simplifiedlinkedin.data.model.Post
import com.rohan.simplifiedlinkedin.presentation.ui.fragment.PostDetailsFragment
import com.rohan.simplifiedlinkedin.utils.Constants.POST_DETAILS

class PostDetailsActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_post_details)

        setActionBar()

        val postDetailsFragment =
            PostDetailsFragment.newInstance(intent?.getSerializableExtra(POST_DETAILS) as Post)
        supportFragmentManager.beginTransaction()
            .add(R.id.containerLayout, postDetailsFragment).commit()

    }

    private fun setActionBar() {
        setSupportActionBar(findViewById(R.id.toolbar))

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        if (item.itemId == android.R.id.home) {
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}