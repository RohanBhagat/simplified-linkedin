package com.rohan.simplifiedlinkedin.presentation.ui.fragment

import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.app.PendingIntent
import android.content.Context.INPUT_METHOD_SERVICE
import android.content.Intent
import android.content.IntentSender.SendIntentException
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.auth.api.credentials.Credential
import com.google.android.gms.auth.api.credentials.Credentials
import com.google.android.gms.auth.api.credentials.CredentialsOptions
import com.google.android.gms.auth.api.credentials.HintRequest
import com.rohan.simplifiedlinkedin.R
import com.rohan.simplifiedlinkedin.data.Resource
import com.rohan.simplifiedlinkedin.databinding.FragmentLoginBinding
import com.rohan.simplifiedlinkedin.presentation.ui.activity.MainActivity
import com.rohan.simplifiedlinkedin.presentation.viewmodel.LoginViewModel
import com.rohan.simplifiedlinkedin.presentation.viewmodel.LoginViewModelFactory
import com.rohan.simplifiedlinkedin.utils.CommonFunctions
import com.rohan.simplifiedlinkedin.utils.UserSession
import javax.inject.Inject

const val RESOLVE_HINT = 100

class LoginFragment : BaseFragment(), View.OnClickListener {

    private lateinit var binding: FragmentLoginBinding

    private lateinit var loginViewModel: LoginViewModel

    @Inject
    lateinit var loginViewModelFactory: LoginViewModelFactory

    @Inject
    lateinit var userSession: UserSession

    private var isFirstClick = true

    companion object {
        @JvmStatic
        fun newInstance() = LoginFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        loginViewModel =
            ViewModelProvider(this, loginViewModelFactory).get(LoginViewModel::class.java)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setClickAndTouchListeners()

        observeData()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setClickAndTouchListeners() {

        binding.btLogin.setOnClickListener(this)

        binding.etMobileNumber.setOnTouchListener { _, _ ->
            if (isFirstClick) {
                isFirstClick = false
                requestPhoneNumber()
                binding.etMobileNumber.requestFocus()
            }
            false
        }
    }

    private fun openMainActivity() {
        val intent = Intent(this.context, MainActivity::class.java)
        this.context?.startActivity(intent)

        finishActivity()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bt_login -> {
                hideKeyboard()
                loginButtonClick()
            }
        }
    }

    private fun loginButtonClick() {

        val mobileNumber = binding.etMobileNumber.text.trim()
        val password = binding.etPassword.text.trim()

        when {
            mobileNumber.isEmpty() -> {
                Toast.makeText(
                    this.context,
                    getString(R.string.please_enter_mobile_number),
                    Toast.LENGTH_SHORT
                ).show()
            }
            !CommonFunctions.isValidIndianMobileNumber(mobileNumber.toString()) -> {
                Toast.makeText(
                    this.context,
                    getString(R.string.enter_valid_mobile_number),
                    Toast.LENGTH_SHORT
                ).show()
            }
            password.isEmpty() -> {
                Toast.makeText(
                    this.context,
                    getString(R.string.please_enter_password),
                    Toast.LENGTH_SHORT
                ).show()
            }
            password.length < 5 -> {
                Toast.makeText(
                    this.context,
                    getString(R.string.password_length),
                    Toast.LENGTH_SHORT
                ).show()
            }
            else -> {

                if (CommonFunctions.isNetworkConnected(this.context))
                    loginViewModel.validateCredentials(mobileNumber.toString(), password.toString())
                else
                    Toast.makeText(
                        this.context,
                        getString(R.string.no_internet),
                        Toast.LENGTH_SHORT
                    ).show()
            }
        }
    }

    private fun observeData() {
        loginViewModel.isValidCredentials.observe(viewLifecycleOwner, { response ->

            if (response != null) {
                when (response.status) {
                    Resource.Status.LOADING -> {
                        startLoader()
                    }

                    Resource.Status.SUCCESS -> {
                        response.data?.let {
                            if (it) {
                                loginViewModel.getUserInfo(
                                    binding.etMobileNumber.text.trim().toString()
                                )
                            } else {
                                stopLoader()
                                Toast.makeText(
                                    this.context,
                                    getString(R.string.wrong_credentials),
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    }

                    Resource.Status.ERROR -> {
                        stopLoader()
                        Toast.makeText(
                            this.context,
                            getString(R.string.some_error_occurred),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        })

        loginViewModel.userInfo.observe(viewLifecycleOwner, { response ->

            if (response != null) {
                when (response.status) {
                    Resource.Status.LOADING -> {

                    }

                    Resource.Status.SUCCESS -> {
                        stopLoader()
                        response.data?.let {
                            userSession.saveUserInfo(it)
                            Toast.makeText(
                                this.context,
                                "Welcome ${it.userName}",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        openMainActivity()
                    }

                    Resource.Status.ERROR -> {
                        stopLoader()
                        Toast.makeText(
                            this.context,
                            getString(R.string.some_error_occurred),
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            }
        })
    }

    private fun finishActivity() {
        activity?.finish()
    }

    // Phone Number Picker
    private fun requestPhoneNumber() {
        val hintRequest: HintRequest = HintRequest.Builder()
            .setPhoneNumberIdentifierSupported(true)
            .build()

        val credentialsOptions = CredentialsOptions.Builder()
            .forceEnableSaveDialog()
            .build()

        val intent: PendingIntent? = activity?.let {
            Credentials.getClient(it, credentialsOptions).getHintPickerIntent(
                hintRequest
            )
        }

        try {
            startIntentSenderForResult(
                intent?.intentSender,
                RESOLVE_HINT, null, 0, 0, 0, null
            )
        } catch (e: SendIntentException) {
            e.printStackTrace()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RESOLVE_HINT) {
            if (resultCode == RESULT_OK) {
                val credential: Credential? = data?.getParcelableExtra(Credential.EXTRA_KEY)
                var mobileNumber: String = credential?.id ?: ""

                if (mobileNumber.contains("-")) {
                    mobileNumber.replace("-", "")
                }
                mobileNumber = mobileNumber.substring(mobileNumber.length - 10, mobileNumber.length)

                binding.etMobileNumber.setText(mobileNumber)
                binding.etMobileNumber.setSelection(mobileNumber.length)
            } else {
                showKeyboard()
            }
        }
    }

    private fun showKeyboard() {
        val imm: InputMethodManager =
            this.context?.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(binding.etMobileNumber, InputMethodManager.SHOW_FORCED)
    }

    private fun hideKeyboard() {
        val imm = this.context?.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(binding.etMobileNumber.windowToken, 0)
    }

    private fun startLoader() {
        CommonFunctions.showViews(binding.transparentView, binding.progressBar)
        binding.btLogin.isEnabled = false
    }

    private fun stopLoader() {
        CommonFunctions.hideViews(binding.transparentView, binding.progressBar)
        binding.btLogin.isEnabled = true
    }
}