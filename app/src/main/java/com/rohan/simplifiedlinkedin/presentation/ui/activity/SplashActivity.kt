package com.rohan.simplifiedlinkedin.presentation.ui.activity

import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.lifecycleScope
import com.rohan.simplifiedlinkedin.R
import com.rohan.simplifiedlinkedin.utils.UserSession
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

class SplashActivity : BaseActivity() {

    @Inject
    lateinit var userSession: UserSession

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_splash)

        checkForRedirection()
    }

    private fun checkForRedirection() {
        val intent: Intent = if (userSession.isUserLoggedIn()) {
            Intent(this, MainActivity::class.java)
        } else {
            Intent(this, LoginActivity::class.java)
        }

        lifecycleScope.launch {
            delay(1000)
            startActivity(intent)
            finish()
        }
    }
}