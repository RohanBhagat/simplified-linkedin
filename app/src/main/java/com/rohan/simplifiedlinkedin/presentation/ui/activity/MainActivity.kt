package com.rohan.simplifiedlinkedin.presentation.ui.activity

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.Menu
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import com.rohan.simplifiedlinkedin.R
import com.rohan.simplifiedlinkedin.data.model.Post
import com.rohan.simplifiedlinkedin.databinding.ActivityMainBinding
import com.rohan.simplifiedlinkedin.presentation.ui.fragment.HomeFragment
import com.rohan.simplifiedlinkedin.utils.Constants
import com.rohan.simplifiedlinkedin.utils.UserSession
import javax.inject.Inject


class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding

    @Inject
    lateinit var userSession: UserSession

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this@MainActivity, R.layout.activity_main)

        setActionBar()

        val homeFragment = HomeFragment.newInstance()
        supportFragmentManager.beginTransaction()
            .add(R.id.containerLayout, homeFragment).commit()
    }

    fun openPostDetailsActivity(post: Post) {
        val intent = Intent(this, PostDetailsActivity::class.java)
        intent.putExtra(Constants.POST_DETAILS, post)
        startActivity(intent)
    }

    private fun setActionBar() {
        setSupportActionBar(binding.toolbar)

        supportActionBar?.elevation = 0F

        supportActionBar?.title = "Hi ${
            userSession.getUserData().userName.substring(
                0, userSession.getUserData().userName.indexOf(
                    " "
                )
            )
        }"
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        val item = menu?.getItem(0)
        val s = SpannableString("Logout")
        s.setSpan(ForegroundColorSpan(Color.WHITE), 0, s.length, 0)
        item?.title = s
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_logout) {

            userSession.clearUserSession()

            openLoginActivity()

            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun openLoginActivity() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
}