package com.rohan.simplifiedlinkedin.presentation.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.rohan.simplifiedlinkedin.R
import com.rohan.simplifiedlinkedin.data.Resource
import com.rohan.simplifiedlinkedin.data.model.Post
import com.rohan.simplifiedlinkedin.databinding.FragmentMyPostBinding
import com.rohan.simplifiedlinkedin.presentation.ui.activity.MainActivity
import com.rohan.simplifiedlinkedin.presentation.ui.adapter.PostAdapter
import com.rohan.simplifiedlinkedin.presentation.viewmodel.HomeViewModel
import com.rohan.simplifiedlinkedin.presentation.viewmodel.HomeViewModelFactory
import com.rohan.simplifiedlinkedin.utils.CommonFunctions
import com.rohan.simplifiedlinkedin.utils.UserSession
import kotlinx.android.synthetic.main.layout_error.view.*
import javax.inject.Inject

class MyPostFragment : BaseFragment() {

    private lateinit var binding: FragmentMyPostBinding

    @Inject
    lateinit var homeViewModelFactory: HomeViewModelFactory

    private lateinit var homeViewModel: HomeViewModel

    @Inject
    lateinit var userSession: UserSession

    private var postAdapter: PostAdapter? = null

    @Inject
    lateinit var activity: MainActivity

    companion object {
        @JvmStatic
        fun newInstance() = MyPostFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        homeViewModel =
            ViewModelProvider((parentFragment as HomeFragment), homeViewModelFactory).get(
                HomeViewModel::class.java
            )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_post, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (CommonFunctions.isNetworkConnected(this.context))
            homeViewModel.getMyPost(userSession.getUserData().userId)
        else
            showErrorViews(R.string.no_internet, R.drawable.ic_no_internet)

        observeData()
    }

    private fun openPostDetailActivity(post: Post) {
        activity.openPostDetailsActivity(post)
    }

    private fun observeData() {
        homeViewModel.myPosts.observe(viewLifecycleOwner, { response ->
            if (response != null) {
                when (response.status) {
                    Resource.Status.LOADING -> {
                        startShimmer()
                    }

                    Resource.Status.SUCCESS -> {
                        response.data?.let {
                            if (response.data.isNotEmpty()) {

                                stopShimmer()
                                CommonFunctions.showViews(binding.rvMyPost)
                                CommonFunctions.hideViews(binding.layoutError)
                                if (postAdapter == null) {
                                    postAdapter = PostAdapter(::openPostDetailActivity)
                                    binding.rvMyPost.adapter = postAdapter
                                    binding.rvMyPost.setHasFixedSize(true)
                                }
                                postAdapter?.submitList(response.data)
                            } else {
                                showErrorViews(R.string.no_post_available, R.drawable.ic_no_posts)
                            }
                        }
                    }

                    Resource.Status.ERROR -> {
                        showErrorViews(R.string.some_error_occurred, R.drawable.ic_error)
                    }
                }
            }
        })
    }

    private fun showErrorViews(errorMessageId: Int, drawableId: Int) {
        stopShimmer()
        CommonFunctions.showViews(binding.layoutError)
        CommonFunctions.hideViews(binding.rvMyPost)
        binding.layoutError.tv_error_message.text = getString(errorMessageId)
        binding.layoutError.iv_icon.setImageResource(drawableId)
    }

    private fun startShimmer() {
        CommonFunctions.showViews(binding.layoutShimmerView.shimmerView)
        CommonFunctions.hideViews(binding.rvMyPost, binding.layoutError)
        binding.layoutShimmerView.shimmerView.startShimmer()
    }

    private fun stopShimmer() {
        CommonFunctions.hideViews(binding.layoutShimmerView.shimmerView)
        binding.layoutShimmerView.shimmerView.stopShimmer()
    }
}