package com.rohan.simplifiedlinkedin.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rohan.simplifiedlinkedin.domain.interfaces.PostDetailsRepository
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Named

class PostDetailsViewModelFactory @Inject constructor(
    @Named("IO") private var io: CoroutineDispatcher,
    @Named("MAIN") private var main: CoroutineDispatcher,
    private val postDetailsRepository: PostDetailsRepository
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PostDetailsViewModel::class.java)) {
            return PostDetailsViewModel(io, main, postDetailsRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
