package com.rohan.simplifiedlinkedin.presentation.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.rohan.simplifiedlinkedin.domain.interfaces.LoginRepository
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import javax.inject.Named

class LoginViewModelFactory @Inject constructor(
    @Named("IO") private var io: CoroutineDispatcher,
    @Named("MAIN") private var main: CoroutineDispatcher,
    private val loginRepository: LoginRepository
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {
            return LoginViewModel(io, main, loginRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
