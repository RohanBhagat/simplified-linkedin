package com.rohan.simplifiedlinkedin.presentation.ui.fragment

import dagger.android.support.DaggerFragment

abstract class BaseFragment : DaggerFragment()