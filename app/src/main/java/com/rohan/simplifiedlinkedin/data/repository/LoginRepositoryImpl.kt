package com.rohan.simplifiedlinkedin.data.repository

import com.rohan.simplifiedlinkedin.data.Resource
import com.rohan.simplifiedlinkedin.data.model.User
import com.rohan.simplifiedlinkedin.data.remote.ApiService
import com.rohan.simplifiedlinkedin.domain.interfaces.LoginRepository
import com.rohan.simplifiedlinkedin.utils.CheckCredentials
import com.rohan.simplifiedlinkedin.utils.Constants.GET_USER_INFO_API_PATH
import kotlinx.coroutines.delay
import javax.inject.Inject

class LoginRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
    private val checkCredentials: CheckCredentials
) : LoginRepository {

    override suspend fun getUserInfo(mobileNumber: String): Resource<User> {
        delay(500)
        return Resource.success(apiService.getUserInfo(mobileNumber, GET_USER_INFO_API_PATH))
    }

    override suspend fun validateCredentials(
        mobileNumber: String,
        password: String
    ): Resource<Boolean> {
        delay(500)
        return Resource.success(checkCredentials.isCredentialsValid(mobileNumber, password))
    }


}