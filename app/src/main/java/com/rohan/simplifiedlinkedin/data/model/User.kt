package com.rohan.simplifiedlinkedin.data.model

import com.google.gson.annotations.SerializedName

data class User(
        @SerializedName("user_id") val userId: String = "",
        @SerializedName("mobile_number") val mobileNumber: String = "",
        @SerializedName("user_name") val userName: String = "",
        @SerializedName("user_email") val userEmail: String = "",
        @SerializedName("current_organization") val currentOrganization: String = "",
        @SerializedName("highest_education") val highestEducation: String = "",
        @SerializedName("gender") val gender: String = ""
)
