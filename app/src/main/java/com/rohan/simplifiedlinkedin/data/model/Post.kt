package com.rohan.simplifiedlinkedin.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class Post(
        @SerializedName("date") val postDate: String = "",
        @SerializedName("posted_by") val postedBy: String = "",
        @SerializedName("message") val message: String = "",
        @SerializedName("user_id") val userId: String = "",
        @SerializedName("post_id") val postId: String = "",
        @SerializedName("comments_count") val commentsCount: Int = 0,
        @SerializedName("likes_count") val likesCount: Int = 0,
        @SerializedName("gender") val gender: String = "",
        @SerializedName("post_owner_organization") val postOwnerOrganization: String = ""
) : Serializable
