package com.rohan.simplifiedlinkedin.data.model

import com.google.gson.annotations.SerializedName

class CommentsEntity {
    @SerializedName("allComments")
    val allComments: List<Comment> = listOf()
}