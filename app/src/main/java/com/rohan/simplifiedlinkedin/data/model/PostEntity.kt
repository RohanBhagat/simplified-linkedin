package com.rohan.simplifiedlinkedin.data.model

import com.google.gson.annotations.SerializedName

class PostEntity {
    @SerializedName("posts")
    val posts: List<Post> = listOf()
}