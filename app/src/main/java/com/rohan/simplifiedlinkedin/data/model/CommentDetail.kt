package com.rohan.simplifiedlinkedin.data.model

import com.google.gson.annotations.SerializedName

data class CommentDetail(
        @SerializedName("date") val commentDate: String = "",
        @SerializedName("commented_by") val commentedBy: String = "",
        @SerializedName("comment") val comment: String = "",
        @SerializedName("user_id") val userId: String = "",
        @SerializedName("post_id") val postId: String = "",
        @SerializedName("gender") val gender: String = "",
        @SerializedName("comment_id") val commentId: String = ""
)
