package com.rohan.simplifiedlinkedin.data.model

import com.google.gson.annotations.SerializedName

data class Comment(
        @SerializedName("post_id") val postId: String = "",
        @SerializedName("comments") val commentsList: List<CommentDetail> = listOf()
)
