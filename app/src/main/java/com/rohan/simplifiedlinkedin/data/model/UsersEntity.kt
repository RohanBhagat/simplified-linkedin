package com.rohan.simplifiedlinkedin.data.model

import com.google.gson.annotations.SerializedName

data class UsersEntity(
        @SerializedName("users") val usersList: List<User> = listOf()
)
