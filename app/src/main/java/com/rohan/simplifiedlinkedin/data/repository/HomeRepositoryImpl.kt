package com.rohan.simplifiedlinkedin.data.repository

import com.rohan.simplifiedlinkedin.data.Resource
import com.rohan.simplifiedlinkedin.data.model.Post
import com.rohan.simplifiedlinkedin.data.remote.ApiService
import com.rohan.simplifiedlinkedin.domain.interfaces.HomeRepository
import com.rohan.simplifiedlinkedin.utils.Constants.GET_FEED_API_PATH
import kotlinx.coroutines.delay
import javax.inject.Inject

class HomeRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
) : HomeRepository {

    override suspend fun getMyFeed(userId: String): Resource<List<Post>> {
        delay(500)
        return Resource.success(apiService.getFeed(userId, GET_FEED_API_PATH))
    }

    override suspend fun getMyPosts(userId: String): Resource<List<Post>> {
        delay(500)
        return Resource.success(apiService.getMyPosts(userId, GET_FEED_API_PATH))
    }
}