package com.rohan.simplifiedlinkedin.data.remote

import android.content.Context
import com.google.gson.Gson
import com.rohan.simplifiedlinkedin.data.model.*
import com.rohan.simplifiedlinkedin.utils.ReadJsonFileHelper
import javax.inject.Inject

class ApiServiceImpl @Inject constructor(
    private val context: Context,
    private val gson: Gson
) : ApiService {

    override suspend fun getFeed(userId: String, apiPath: String): List<Post> {
        val postEntityModel = gson.fromJson(readJsonFile(apiPath), PostEntity::class.java)

        val postList = mutableListOf<Post>()

        if (postEntityModel != null && postEntityModel.posts.isNotEmpty()) {

            for (post in postEntityModel.posts) {
                if (post.userId != userId) {
                    postList.add(post)
                }
            }
        }

        return postList
    }

    override suspend fun getMyPosts(userId: String, apiPath: String): List<Post> {

        val postEntityModel = gson.fromJson(readJsonFile(apiPath), PostEntity::class.java)

        val postList = mutableListOf<Post>()

        if (postEntityModel != null && postEntityModel.posts.isNotEmpty()) {

            for (post in postEntityModel.posts) {
                if (post.userId == userId) {
                    postList.add(post)
                }
            }
        }

        return postList
    }

    override suspend fun getComments(postId: String, apiPath: String): List<CommentDetail> {

        val commentsEntityModel = gson.fromJson(readJsonFile(apiPath), CommentsEntity::class.java)

        if (commentsEntityModel != null && commentsEntityModel.allComments.isNotEmpty()) {

            for (comment in commentsEntityModel.allComments) {
                if (comment.postId == postId) {
                    return comment.commentsList
                }
            }
        }

        return listOf()
    }

    override suspend fun getUserInfo(mobileNumber: String, apiPath: String): User {

        val userEntityModel = gson.fromJson(readJsonFile(apiPath), UsersEntity::class.java)

        if (userEntityModel != null && userEntityModel.usersList.isNotEmpty()) {

            for (user in userEntityModel.usersList) {
                if (user.mobileNumber == mobileNumber) {
                    return user
                }
            }
        }

        return User(mobileNumber = mobileNumber)
    }

    private fun readJsonFile(apiPath: String) = ReadJsonFileHelper.readJsonFile(context, apiPath)
}