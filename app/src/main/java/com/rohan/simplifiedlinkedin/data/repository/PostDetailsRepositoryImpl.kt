package com.rohan.simplifiedlinkedin.data.repository

import com.rohan.simplifiedlinkedin.data.Resource
import com.rohan.simplifiedlinkedin.data.model.CommentDetail
import com.rohan.simplifiedlinkedin.data.remote.ApiService
import com.rohan.simplifiedlinkedin.domain.interfaces.PostDetailsRepository
import com.rohan.simplifiedlinkedin.utils.Constants.GET_COMMENTS_DETAIL_API_PATH
import kotlinx.coroutines.delay
import javax.inject.Inject

class PostDetailsRepositoryImpl @Inject constructor(
    private val apiService: ApiService,
) : PostDetailsRepository {

    override suspend fun getComments(postId: String): Resource<List<CommentDetail>> {
        delay(500)
        return Resource.success(apiService.getComments(postId, GET_COMMENTS_DETAIL_API_PATH))
    }
}