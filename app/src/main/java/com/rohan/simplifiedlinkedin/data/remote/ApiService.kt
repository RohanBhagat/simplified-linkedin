package com.rohan.simplifiedlinkedin.data.remote

import com.rohan.simplifiedlinkedin.data.model.CommentDetail
import com.rohan.simplifiedlinkedin.data.model.Post
import com.rohan.simplifiedlinkedin.data.model.User

interface ApiService {

    suspend fun getFeed(userId: String, apiPath: String): List<Post>

    suspend fun getMyPosts(userId: String, apiPath: String): List<Post>

    suspend fun getComments(postId: String, apiPath: String): List<CommentDetail>

    suspend fun getUserInfo(mobileNumber: String, apiPath: String): User
}