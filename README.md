# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a sample app which demonstrates Clean Architecture using MVVM.

It has the following Tech stack:

* ViewModel
* Dagger
* Coroutines
* LiveData
* ViewPager
* MVVM architecture

![Screenshot 1](images/Screenshot1.jpeg)
![Screenshot 2](images/Screenshot2.jpeg)
![Screenshot 3](images/Screenshot3.jpeg)
![Screenshot 4](images/Screenshot4.jpeg)
![Screenshot 5](images/Screenshot5.jpeg)

### How do I get set up? ###

Simply clone the repository in Android Studio and run on a device or emulator

### What is the architecture? ###

Clean Architecture has been used in the codebase. Following are the packages:

* **data**: It contains the repository which is responsible to get data through different sources (if needed), it has all the required models needed for api calling.
    
* **di**: Contains classes needed for dependency injection having components, modules.
    -Technologies: Dagger2

* **presentation**: The presentation layer consists of the ui and the view models
    - ui: the UI part consists of Activities, Fragments, Adapters, BaseActivity, BaseFragment
    - viewmodel
    
    - Technologies: ViewModel, Coroutines, LiveData, Dagger2
